class ContactsController < ApplicationController

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
  
    if @contact.deliver
     # flash[:notice] = "Thank you for your message. We'll get back to you shortly!"
      render 'popup'
    else
      flash[:danger] = "There was a problem sending your message. Please try again!"
    end
  end

  def edit
    @contact_params = Contact.find(params[:id])
  end

  private
  def contact_params
    params.require(:contact).permit(:name, :email, :message)
  end
end
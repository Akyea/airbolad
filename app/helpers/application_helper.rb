module ApplicationHelper

  def avatar_url(user, size_1=150, size_2=150)
    if user.image
      "https://graph.facebook.com/#{user.uid}/picture?type=large"
      #{}"https://graph.facebook.com/<userId>/?fields=picture&type=large&access_token=b52be08df38288f1480e3b0b9d189c2f"
      #"https://graph.facebook.com/v2.12/#{user.uid}/picture?height=100&width=100&access_token=b52be08df38288f1480e3b0b9d189c2f"
    elsif user.avatar.attached?
      user.avatar.variant(resize: "#{size_1}x#{size_2}!")
    else
      gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
      "https://www.gravatar.com/avatar/#{gravatar_id}.jpg?d=identicon&s=150"
    end
  end

  # def user_avatar(user, size_1=150, size_2=150)
  #   if user.avatar.attached?
  #     user.avatar.variant(resize: "#{size_1}x#{size_2}!")
  #   else
  #     '/unknown.jpg'
  #   end
  # end

end
